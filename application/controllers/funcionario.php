<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');
class Funcionario extends CI_Controller{
	public function ferias(){
		$this->load->model('Ferias_funcionario');
		$data['titulo'] = 'Gerenciamento de férias';
		$funcionarios = $this->Ferias_funcionario->findAll();

		array_walk($funcionarios, function(&$item){
			$date = new DateTime($item['data_contratacao']);
			
			$item['data_contratacao'] = $date;

			$fim_aquisitivo = new DateTime($item['data_contratacao']->format('Y-m-d'));
			$fim_aquisitivo->add(new DateInterval('P10D'));
			$item['fim_aquisitivo'] = $fim_aquisitivo;
							
			$concessivo = new DateTime($item['fim_aquisitivo']->format('Y-m-d'));
			$concessivo->add(new DateInterval('P10D'));
			$item['periodo_convessivo'] = $concessivo;
			return $item;
		});
		$data['funcionarios'] = $funcionarios;

		$this->load->view('funcionario/ferias', $data);
	}

	
}