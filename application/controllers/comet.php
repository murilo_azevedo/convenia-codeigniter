<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');
class Comet extends CI_Controller{
	
	protected $charsValues = array();
    protected $mod = 45;

    public function __construct(){
        $currentCharValue = 1;
        // 97 - char para a letra a
        // 122 para a letra z
        for ($i=97; $i <= 122; ++$i) {
            $this->charsValues[chr($i)] = $currentCharValue;
            $currentCharValue++;
        }
        parent::__construct();
    }
	public function index(){
		$this->load->model('comet_developer');
		$groups = $this->comet_developer->findAll();
		foreach($groups as &$group){
		    if( !$this->isMatch($group['comet'], $group['group']) ){
		    	$group['taken'] = false;
		    }else{
		    	$group['taken'] = true;
		    }
		        
		}
		$this->load->view('comet/index', array(
			'groups' => $groups,
			'titulo' => 'Procurando devs'
		));
	}
	private function _valueFromWord($word){
		// Converte o array para lower case e separa ele em um array
        $word = str_split(strtolower($word));
        // Aplica uma função que retorna o valor de cada caracter em um array
        $word = array_map(array($this, 'charValue'), $word);

        // Pega o primeiro valor para começar a multiplicação
        $value = current($word);
        // Varre o array de valores multiplicando o valor atual com o proximo numero
        for ($i=1; $i < count($word) ; $i++) { 
            $value = $value * $word[$i];
        }
        
        return $value;
	}
	private function charValue($char){
        return $this->charsValues[$char];
    }
	 private function isMatch($comet, $group){
        $comet = $this->_valueFromWord($comet);
        $group = $this->_valueFromWord($group);
        return ($this->getMod($comet) == $this->getMod($group));
    }

    private function getMod($value){
        return $value % $this->mod;
    }
}