<?php
class Comet_developer extends CI_Model{
	
	public function findAll(){
		return $this->db->select()
		->from('comet_developers')
		->get()
		->result_array();
	}
}