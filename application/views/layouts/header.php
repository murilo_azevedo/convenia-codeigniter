<!DOCTYPE html>
<html lang="en">
<head>
<meta charset="utf-8">
<meta name="description" content="Automatize todas as funções burocráticas do RH e concentre-se apenas na parte estratégica da gestão de pessoas.">
	<meta name="keywords" content="RH, gestão de pessoas, automação, Gestão RH, altomação, RH gestão, gestao rh, convenia RH">
	<meta name="language" content="pt-br" />
	<meta name="robots" content="index, follow" />
	<meta property="og:title" content="Convenia - Automação do RH" />
	<meta property="og:description" content="Automatize todas as funções burocráticas do RH e concentre-se apenas na parte estratégica da gestão de pessoas." />
	<meta property="og:image" content="https://rh.convenia.com.br/assets/dashboard/img/facethumb.jpg" />

<title>Convenia - <?php echo $titulo?></title>
	<link rel="shortcut icon" href="<?php echo base_url('assets/img/favicon.ico')?>" />
	<?php echo link_tag('assets/css/bootstrap.min.css'); ?>
	<?php echo link_tag('assets/css/font-awesome.min.css')?>
	<?php echo link_tag('assets/css/awesome-bootstrap-checkbox.css')?>
	<?php echo link_tag('assets/css/base.css')?>
	
</head>
<body>

<div class="container">
<nav class="navbar navbar-default">
  <div class="container-fluid">
    <!-- Brand and toggle get grouped for better mobile display -->
    <div class="navbar-header">
      <a class="navbar-brand" href="#">Convenia</a>
    </div>
     <!-- Collect the nav links, forms, and other content for toggling -->
    <div class="collapse navbar-collapse" id="bs-example-navbar-collapse-1">
      <ul class="nav navbar-nav">
        <li class="active"><a href="<?php echo base_url('/')?>">Busca de desenvoledores</a></li>
        <li><a href="<?php echo base_url('/ferias')?>">Gerenciamento de férias</a></li>
      </ul>
    </div>
  </div>
</nav>