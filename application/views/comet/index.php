
<?php $this->load->view('layouts/header')?>
<table class="table table-condensed table-striped">
	<h1>OVNI's buscam desenvolvedores!</h1>
	<thead>
		<th>Cometa</th>
		<th>Grupo</th>
		<th>Será levado</th>
	</thead>
	<tbody>
		<?php foreach ($groups as $group):?>
			<tr class="<?php echo $group['taken'] ? 'levado': 'danger'?>">
				<td><?php echo $group['comet']?></td>
				<td><?php echo $group['group']?></td>
				<td><?php echo $group['taken'] ? 'Sim': 'Não' ?></td>
			</tr>
		<?php endforeach;?>
	</tbody>
	<tfoot>
		<th>Cometa</th>
		<th>Grupo</th>
		<th>Será levado</th>
	</tfoot>
</table>


<?php $this->load->view('layouts/footer')?>