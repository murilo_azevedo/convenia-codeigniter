
<?php $this->load->view('layouts/header')?>
<table class="table table-condensed table-striped">
	<h1>Gerenciamento de férias</h1>
	<thead>
		<th>Funcionário</th>
		<th>Período acquisitivo</th>
		<th>Período concessivo</th>
	</thead>
	<tbody>
		<?php foreach ($funcionarios as $funcionario):?>
			<tr>
				<td><?php echo $funcionario['nome']?></td>
				<td><?php echo $funcionario['data_contratacao']->format('d/m/Y') . ' - ' .$funcionario['fim_aquisitivo']->format('d/m/Y')?></td>
				<td><?php echo $funcionario['fim_aquisitivo']->format('d/m/Y') . ' - ' .$funcionario['periodo_convessivo']->format('d/m/Y')?></td>
			</tr>
		<?php endforeach;?>
	</tbody>
	<tfoot>
		<th>Funcionário</th>
		<th>Período acquisitivo</th>
		<th>Período concessivo</th>
	</tfoot>
</table>


<?php $this->load->view('layouts/footer')?>